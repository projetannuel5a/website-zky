# README #
### Page d'administration : ###
* Module d'upload de csv de produits
* Module d'upload de csv de clients
* Module de modification des informations affichés dans la page de contact
* Module de visionnage de la liste des commandes en cours de traitement et terminés (bouton select pour choisir le statut)

### Page de catalogue : ###
* Liste de previews des produits mise à jour lorsqu'on change les filtres
* Cliquer sur une preview amène vers la fiche produit
* filtre :Bouton select avec la liste des catégories
* filtre :Bouton select dynamique avec la liste des sous-catégories de la catégorie en cours
* filtre :Champ de recherche
* Pagination avec 20 produits par page

### Page de fiche produit : ###
* Fil d'ariane avec lien cliquable pour la catégorie et sous-catégorie du produit
* Image, nom, référence et prix du produit
* choix de la quantité et bouton d'ajout à la commande
* bouton pour revenir au catalogue, gardant en mémoire les derniers filtres

### Panneau de la commande : ###
* Affichage de la liste des produits dans la commande
* Modifier la quantitée
* Prix unitaire, prix en fonction de la quantité et prix total de la commande
* Bouton pour envoyer sa commande

### Page de configuration du compte : ###
* Module d'historique des commandes avec status, le clic sur une commande affiche les détails
* Module changement de mot de passe
* Module de formulaire de changement d'informations


### Page de connexion : ###
* formulaire de connexion
* lien "nouveau client" vers la page de contact
* lien "mot de passe oublié" vers le formulaire de réinitialisation de mot de passe

### Page de réinitialisation de mot de passe : ###
* Formulaire avec email du compte à réinitialiser (confirmation par mail via lien avec token)

### Header: ###
* Lien vers l'accueil, le catalogue et la page de contact
* Lien vers la page de connexion si non connecté
* Lien vers la déconnexion et la configuration du compte si connecté
* Lien vers la page d'administration si connecté en temps qu'admin

### Footer : ###
* Génération dynamique du sitemap avec liens pour la liste des catégories et sous catégories

### Page d'accueil : ###
* Message d'introduction
* Bouton "devenir client" amène vers la page de contact

### Page de contact : ###
* Message d'intro et numéro de téléphone, adresse email et adresse postale de contact

### Page détail de commande : ###
* Information du client
* Information sur la commande
* Liste des produits et leurs quantité
* Changer le statut de la commande si connecté en temps qu’admin