/**
 * Created by Andreï for ZKY.
 */
 import { Component, Input, OnDestroy, OnInit } from '@angular/core';
 
 import { ToasterService } from '../_services/toaster.service';
 import { Toast } from '../_models/toast';
 import { Subscription }   from 'rxjs/Subscription';
 
 @Component({
     selector: 'toast',
     templateUrl: './toast.component.html',
     styleUrls: ['./toast.component.scss'],
     providers: [ToasterService]
 })
 export class ToastComponent implements OnDestroy, OnInit {
     toast:Toast;
     subscription: Subscription;
     
     constructor(private toasterService: ToasterService) {
         this.subscription = ToasterService.toast$.subscribe(
             toast => {
                 this.toast = toast;
             });
     }
     
     ngOnInit() {
         ToasterService.init();
     }

     ngOnDestroy() {
         // prevent memory leak when component destroyed
         this.subscription.unsubscribe();
     }
 }