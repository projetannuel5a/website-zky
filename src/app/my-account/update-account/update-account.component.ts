/**
 * Created by Andreï for ZKY.
 */
 import {Component, OnInit} from '@angular/core';
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'

 import { AlertService } from '../../_services/alert.service';
 import { AuthenticationService } from '../../_services/authentication.service';
 import { ToasterService } from '../../_services/toaster.service';

 import { Cart } from '../../_models/cart';
 import { ChangePasswordInfos } from '../../_models/changePasswordInfos';
 import { Account } from '../../_models/account';


 @Component({
     selector: 'update-account',
     styleUrls: ['./update-account.component.scss'],
     templateUrl: './update-account.component.html',
     providers:[AuthenticationService,AlertService,ToasterService]
 })
 export class UpdateAccountComponent {
     constructor( 
         private authenticationService: AuthenticationService,
         private _router: Router,
         private alertService: AlertService
         ){}

     private account : Account = AuthenticationService.currentUser;

     private onClickSend( id ): void {
         this.authenticationService.updateAccount( this.account ).then( contactInfos => {
             ToasterService.popToast("info","Informations du compte mises à jour");
         }).catch( error =>{
             ToasterService.popToast("error","Erreur lors de la mise à jour des informations du compte");
         });;
     }
 }