/**
 * Created by Andreï for ZKY.
 */
 import {Component, OnInit} from '@angular/core';
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'

 import { AlertService } from '../../_services/alert.service';
 import { AuthenticationService } from '../../_services/authentication.service';
 import { ToasterService } from '../../_services/toaster.service';

 import { Cart } from '../../_models/cart';
 import { ChangePasswordInfos } from '../../_models/changePasswordInfos';


 @Component({
     selector: 'change-password',
     styleUrls: ['./change-password.component.scss'],
     templateUrl: './change-password.component.html',
     providers:[AuthenticationService,AlertService]
 })
 export class ChangePasswordComponent {
     constructor( 
         private authenticationService: AuthenticationService,
         private _router: Router,
         private alertService: AlertService
         ){}

     private changePasswordInfos : ChangePasswordInfos= {
         oldPassword: "",
         newPassword: ""
     }

     private onClickSend( id ): void {
         this.authenticationService.changePassword( this.changePasswordInfos ).then( contactInfos => {
             ToasterService.popToast("info","Mot de passe mis à jour");
         }).catch( error =>{
             ToasterService.popToast("error","Ancien mot de passe incorrect");
         });
     }
 }