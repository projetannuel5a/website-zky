/**
 * Created by Andreï for ZKY.
 */
 import {Component} from '@angular/core';

 @Component({
     selector: 'my-account',
     styleUrls: ['./my-account.component.scss'],
     templateUrl: './my-account.component.html'
 })
 export class MyAccountComponent {
 }
