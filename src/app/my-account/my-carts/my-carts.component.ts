/**
 * Created by Andreï for ZKY.
 */
 import {Component, OnInit} from '@angular/core';
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'
 import { CartService } from '../../_services/cart.service';
 import { Cart } from '../../_models/cart';


 @Component({
     selector: 'my-carts',
     styleUrls: ['./my-carts.component.scss'],
     templateUrl: './my-carts.component.html',
     providers:[CartService]
 })
 export class MyCartsComponent implements OnInit {
     constructor( 
         private cartService: CartService,
         private _router: Router
         ){}

     private carts:Cart[];

     ngOnInit() {
         this.cartService.getHistory().then( (carts:Cart[]) => this.updateCart(carts) ).catch( error =>{
             console.error("Erreur lors de la demande des anciennes commandes");
         });
     }

     private updateCart( carts:Cart[]){
         this.carts = [];
         for( let i = 0 ; i < carts.length ; i++ ){
             this.carts.push( new Cart( carts[i] ) );
         }
     }

     private onClickPreview( id ): void {
         this._router.navigate( ['/cart-view', id] );
     }

     private numberTo2Decimals( number : number ){
         return ((number * 100) / 100).toFixed(2);
     }
 }