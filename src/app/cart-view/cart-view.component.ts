/**
 * Created by Andreï for ZKY.
 */
 import 'rxjs/add/operator/switchMap';
 import { ActivatedRoute, Params } from '@angular/router'
 import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core'
 import { Router } from '@angular/router';

 import { CartService } from '../_services/cart.service';
 import { ToasterService } from '../_services/toaster.service';
 import { AuthenticationService } from '../_services/authentication.service';

 import { Cart } from '../_models/cart';

 @Component({
     selector: 'cart-view',
     templateUrl: './cart-view.component.html',
     styleUrls: ['./cart-view.component.scss'],
     providers: [CartService,ToasterService,AuthenticationService]
 })
 export class CartViewComponent implements OnInit {
     constructor(
         private route: ActivatedRoute,
         private _router: Router,
         private cartService: CartService,
         private authenticationService: AuthenticationService
         ) {}

     private cart : Cart;

     private updateCart( cart:Cart ){
         this.cart = new Cart( cart );
     }

     private finishCart() {
         this.cartService.setCartToValidated( this.cart.cart.id ).then(
             response => {
                 this.cartService.getCartById( this.cart.cart.id ).then(
                     (cart) => this.updateCart(cart)
                     ).catch( error =>{
                         console.error("Erreur lors de la mise à jour du panier");
                     });
                     ToasterService.popToast("info","commande marquée comme terminée");
                 }
                 ).catch( error =>{
                     ToasterService.popToast("error","Erreur lors de la modification");
                 });
             }
             private return() {
                 this._router.navigate([localStorage.getItem('previousRoute')]);
             }

             ngOnInit(): void {
                 this.route.params
                 .switchMap((params: Params) => this.cartService.getCartById(+params['id']))
                 .subscribe((cart) => this.updateCart(cart));
             }

             private numberTo2Decimals( number : number ){
                 return ((number * 100) / 100).toFixed(2);
             }
         }