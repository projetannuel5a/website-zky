/**
 * Created by Andreï for ZKY.
 */
 import {Component, trigger, state, style, transition, animate} from '@angular/core';
 import {AuthenticationService} from './_services/authentication.service';

 import { OnInit }              from '@angular/core';
 import { Router }              from '@angular/router';

 import {ViewEncapsulation} from '@angular/core';

 import 'rxjs/add/operator/filter';
 import 'rxjs/add/operator/pairwise';

 @Component({
     selector: 'app',
     encapsulation: ViewEncapsulation.None,
     templateUrl: './app.component.html',
     styleUrls: [
     './app.component.scss',
     './common-buttons.scss',
     './common-forms.scss',
     './common-quantity.scss',
     './common-styles.scss',
     /*'../assets/font-awesome-4.7.0/css/font-awesome.min.css',*/
     ],
     animations: [
     trigger('slideInOut', [
         state('in', style({
             transform: 'translate3d(0, 0, 0)',
             boxShadow: "20px 0 35px 0 rgba(0, 0, 0, .1)"
         })),
         state('out', style({
             transform: 'translate3d(-100%, 0, 0)',
             boxShadow: "20px 0 35px 0 rgba(0, 0, 0, 0)"
         })),
         transition('in => out', animate('400ms ease-in-out')),
         transition('out => in', animate('400ms ease-in-out'))
         ]),
     ],
     providers:[AuthenticationService]
 })
 export class AppComponent {
     constructor( 
         private authenticationService: AuthenticationService,
         private router: Router
         ){}

     private menuState:string = 'out';

     private toggleNavMenu(){
         this.menuState = this.menuState === 'out' ? 'in' : 'out';
     }

     ngOnInit() {
         //save previous route for connection callback
         this.router.events
         .filter(e => e.constructor.name === 'RoutesRecognized')//filter only legal routes
         .pairwise()
         .subscribe((e: any[]) => {
             localStorage.setItem('previousRoute', e[0].urlAfterRedirects);
         });
     }
 }
