/**
 * Created by Andreï for ZKY.
 */
 import { Component, OnInit } from '@angular/core';

 import { ProductsService } from '../_services/product.service';
 import { FamilyAndSubfamilies } from '../_models/familyAndSubfamilies';
 import { SubfamilyAndProducts } from '../_models/subfamilyAndProducts';
 import { Family } from '../_models/family';
 import { Subfamily } from '../_models/subfamily';
 import { QueryParameterParserService } from '../_services/queryParameterParser.service';

 @Component({
     selector: 'app-footer',
     templateUrl: './app-footer.component.html',
     styleUrls: ['./app-footer.component.scss'],
     providers: [ProductsService,QueryParameterParserService]
 })
 export class AppFooterComponent implements OnInit {
     constructor(private productsService: ProductsService, private queryParameterParserService: QueryParameterParserService) {

     }

     private families: FamilyAndSubfamilies[];
     private currentYear: number;
     
     ngOnInit() {
         this.productsService.getFamilies().then(families =>{ 
             this.families = families;
         }).catch( error =>{
             console.error("Erreur lors de la demande des catégories");
         });

         this.currentYear = new Date().getFullYear();
     }

     private getFamilyQueryParameter( family:Family):string{
         return this.queryParameterParserService.encode( {
             family: family.family.toString()
         } );
     }

     private getSubfamilyQueryParameter( family:Family, subfamily:Subfamily):string{
         return this.queryParameterParserService.encode( {
             family: family.family.toString(),
             subfamily: subfamily.subfamily.toString()
         } );
     }
 }
