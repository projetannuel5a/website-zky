/**
 * Created by Andreï for ZKY.
 */
 import { Routes } from '@angular/router';

 import { CatalogueComponent } from './catalogue/catalogue.component';
 import { ProductComponent } from './product/product.component';
 import { ContactComponent } from './contact/contact.component';
 import { ConnectionComponent } from './connection/connection.component';
 import { HomeComponent } from './home/home.component';
 import { MyAccountComponent } from './my-account/my-account.component';
 import { AdminInterfaceComponent } from './admin-interface/admin-interface.component';
 import { CartViewComponent } from './cart-view/cart-view.component';
 import { ForgottenPasswordComponent } from './forgotten-password/forgotten-password.component';

 import { LoggedGuard } from './_guards/logged.guards';
 import { NotLoggedGuard } from './_guards/notLogged.guards';
 import { AdminGuard } from './_guards/admin.guards';


 export const rootRouterConfig: Routes = [
 { path: '', redirectTo: 'home', pathMatch: 'full' },
 
 { path: 'home', component: HomeComponent },
 { path: 'product/:id', component: ProductComponent },
 { path: 'catalogue', component: CatalogueComponent },
 { path: 'catalogue/:params', component: CatalogueComponent },
 { path: 'contact', component: ContactComponent },
 { path: 'forgotten-password', component: ForgottenPasswordComponent },
 { path: 'connection', component: ConnectionComponent, canActivate: [NotLoggedGuard] },
 { path: 'my-account', component: MyAccountComponent, canActivate: [LoggedGuard] },
 { path: 'admin-interface', component: AdminInterfaceComponent, canActivate: [AdminGuard] },
 { path: 'cart-view/:id', component: CartViewComponent, canActivate: [LoggedGuard] },

 { path: '**', redirectTo: 'home' }
 ];

