/**
 * Created by Andreï for ZKY.
 */
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'
 import { Input, Component } from '@angular/core'

 import { Product } from '../_models/product';

 @Component({
     selector: 'products-grid',
     styleUrls: ['./products-grid.component.scss'],
     templateUrl: './products-grid.component.html'
 })
 export class ProductsGridComponent {
     constructor( 
         private _router: Router
         ){

     }

     @Input() private products: Product[];

     private onClickPreview( id ): void {
         this._router.navigate( ['/product', id] );
     }
 }
