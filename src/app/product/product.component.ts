/**
 * Created by Andreï for ZKY.
 */
 import 'rxjs/add/operator/switchMap';
 import { ActivatedRoute, Params } from '@angular/router'
 import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core'
 import { Router } from '@angular/router';

 import { ProductsService } from '../_services/product.service';
 import { CartService } from '../_services/cart.service';

 import { Product } from '../_models/product';
 import { Cart } from '../_models/cart';
 import { FamilyAndSubfamilies } from '../_models/familyAndSubfamilies';
 import { SubfamilyAndProducts } from '../_models/subfamilyAndProducts';
 import { Family } from '../_models/family';
 import { Subfamily } from '../_models/subfamily';

 import {AuthenticationService} from '../_services/authentication.service';
 import { QueryParameterParserService } from '../_services/queryParameterParser.service';
 import { ToasterService } from '../_services/toaster.service';

 @Component({
     selector: 'product',
     templateUrl: './product.component.html',
     styleUrls: ['./product.component.scss'],
     providers: [ProductsService,CartService,QueryParameterParserService,ToasterService]
 })
 export class ProductComponent implements OnInit {
     constructor(
         private route: ActivatedRoute,
         private _router: Router,
         private productsService: ProductsService,
         private cartService: CartService,
         private authenticationService: AuthenticationService, 
         private queryParameterParserService: QueryParameterParserService
         ) {}

     private product : Product;

     @Input() quantity : number = 1;
     @Output() quantityChange = new EventEmitter<number>();

     private quantityDecrement() {
         this.quantity--;
         if(this.quantity < 1 ){
             this.quantity = 1;
         }
         this.quantityChange.emit(this.quantity);
     }

     private quantityIncrement() {
         this.quantity++;
         this.quantityChange.emit(this.quantity);
     }

     private addToCart() {
         if( this.authenticationService.isLogged() ){
             this.cartService.addProductQuantity(this.product.id, this.quantity).then( contactInfos => {
                 ToasterService.popToast("info","Produit ajouté au panier");
             });
         }
         else{
             this._router.navigate( ['/connection'] );

         }
     }

     private returnToCatalogue( id ): void {
         let returnUrl = '/catalogue';
         if( localStorage.getItem('previousRoute').search("/catalogue") === 0 ){
             returnUrl = localStorage.getItem('previousRoute');
         }
         this._router.navigate([returnUrl]);
     }

     ngOnInit(): void {
         this.route.params
         .switchMap((params: Params) => this.productsService.getProduct(+params['id']))
         .subscribe(product => this.product = product);
     }

     private numberTo2Decimals( number : number ){
         return ((number * 100) / 100).toFixed(2);
     }

     private getFamilyQueryParameter( family:Family):string{
         return this.queryParameterParserService.encode( {
             family: family.family.toString()
         } );
     }

     private getSubfamilyQueryParameter( family:Family, subfamily:Subfamily):string{
         return this.queryParameterParserService.encode( {
             family: family.family.toString(),
             subfamily: subfamily.subfamily.toString()
         } );
     }
 }