/**
 * Created by Andreï for ZKY.
 */
 import { Component, OnInit } from '@angular/core';

 import { AuthenticationService } from '../_services/authentication.service';
 import { ToasterService } from '../_services/toaster.service';

 import { ContactInfos } from '../_models/contactInfos';

 @Component({
     selector: 'app-contact',
     templateUrl: './contact.component.html',
     styleUrls: ['./contact.component.scss'],
     providers: [AuthenticationService,ToasterService]
 })
 export class ContactComponent implements OnInit {
     constructor( private authenticationService: AuthenticationService ) {}

     private contactInfos:ContactInfos;
     ngOnInit() {
         this.authenticationService.getContactInfos()
         .then( contactInfos => {
             this.contactInfos = contactInfos;
         }).catch( error =>{
             console.error("Erreur lors de la demande des informations de contact");
         });
     }
 }
