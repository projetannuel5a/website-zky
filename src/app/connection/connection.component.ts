/**
 * Created by Andreï for ZKY.
 */
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params, NavigationEnd } from '@angular/router'
 import { OnInit, Component} from '@angular/core'

 import { AuthenticationService } from '../_services/authentication.service';
 import { ToasterService } from '../_services/toaster.service';
 import { AlertService } from '../_services/alert.service';
 import { ConnectionInfos } from '../_models/connectionInfos';


 @Component({
     selector: 'connection',
     styleUrls: ['./connection.component.scss'],
     templateUrl: './connection.component.html',
     providers: [AuthenticationService, AlertService, ToasterService]
 })
 export class ConnectionComponent {
     constructor( 
         private router: Router,
         private route: ActivatedRoute,
         private authenticationService: AuthenticationService,
         private alertService: AlertService
         ){
     }

     private connectionInfos : ConnectionInfos= {
         login: "",
         password: ""
     }
     connectionSend(){
         this.authenticationService.login(this.connectionInfos.login, this.connectionInfos.password)
         .then( connectionSuccess => {
             if( connectionSuccess ){
                 ToasterService.popToast("info","connexion acceptée");
                 if( localStorage.getItem('previousRoute') && localStorage.getItem('previousRoute').search('connection') === -1 ){
                     this.router.navigate([localStorage.getItem('previousRoute')]);
                 }
                 else{
                     this.router.navigate(["/"]);
                 }
             }
             else{
                 
             }
         })
         .catch( error =>{
             this.alertService.message("error", "Erreur : Login ou mot de passe incorrect")
         });
     }
 }