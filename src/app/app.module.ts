/**
 * Created by Andreï for ZKY.
 */
 import { NgModule } from '@angular/core'
 import { RouterModule } from '@angular/router';
 import { rootRouterConfig } from './app.routes';
 import { AppComponent } from './app.component';
 import { BrowserModule } from '@angular/platform-browser';
 import { HttpModule } from '@angular/http';
 import { FormsModule } from '@angular/forms';
 import { LocationStrategy, HashLocationStrategy } from '@angular/common';

 import { AlertComponent } from './_directives/alert.component';

 import { AppFooterComponent } from './app-footer/app-footer.component';
 import { CatalogueComponent } from './catalogue/catalogue.component';
 import { ProductComponent } from './product/product.component';
 import { ForgottenPasswordComponent } from './forgotten-password/forgotten-password.component';
 import { ProductsGridComponent } from './products-grid/products-grid.component';
 import { PaginationComponent } from './pagination/pagination.component';
 import { ConnectionComponent } from './connection/connection.component';
 import { ContactComponent } from './contact/contact.component';
 import { HomeComponent } from './home/home.component';
 import { AdminInterfaceComponent } from './admin-interface/admin-interface.component';
 import { ProductsManagementComponent } from './admin-interface/products-management/products-management.component';
 import { ClientsManagementComponent } from './admin-interface/clients-management/clients-management.component';
 import { CartsManagementComponent } from './admin-interface/carts-management/carts-management.component';
 import { ChangeContactInfosComponent } from './admin-interface/change-contact-infos/change-contact-infos.component';
 import { CartViewComponent } from './cart-view/cart-view.component';
 import { MyAccountComponent } from './my-account/my-account.component';
 import { MyCartsComponent } from './my-account/my-carts/my-carts.component';
 import { ChangePasswordComponent } from './my-account/change-password/change-password.component';
 import { UpdateAccountComponent } from './my-account/update-account/update-account.component';
 import { CartComponent } from './cart/cart.component';
 import { ToastComponent } from './toast/toast.component';

 import {LoggedGuard} from './_guards/logged.guards';
 import {NotLoggedGuard} from './_guards/notLogged.guards';
 import {AdminGuard} from './_guards/admin.guards';

 import {AuthenticationService} from './_services/authentication.service';

 @NgModule({
     declarations: [
     AppComponent,
     AppFooterComponent,
     AlertComponent,
     CatalogueComponent,
     ProductComponent,
     ForgottenPasswordComponent,
     ProductsGridComponent,
     PaginationComponent,
     ConnectionComponent,
     HomeComponent,
     ContactComponent,
     AdminInterfaceComponent,
     ProductsManagementComponent,
     ClientsManagementComponent,
     CartsManagementComponent,
     ChangeContactInfosComponent,
     CartViewComponent,
     ChangePasswordComponent,
     UpdateAccountComponent,
     MyAccountComponent,
     MyCartsComponent,
     CartComponent,
     ToastComponent
     ],
     imports: [
     BrowserModule,
     HttpModule,
     RouterModule.forRoot(rootRouterConfig, { useHash: true }),
     FormsModule
     ],
     providers: [
     LoggedGuard,
     NotLoggedGuard,
     AdminGuard,
     AuthenticationService
     ],
     bootstrap: [ AppComponent ]
 })

 export class AppModule {

 }
