/**
 * Created by Andreï for ZKY.
 */
 import { Injectable } from '@angular/core';
 import { Router, CanActivate } from '@angular/router';

 import { AuthenticationService } from '../_services/authentication.service';
 
 @Injectable()
 export class NotLoggedGuard implements CanActivate {
     
     constructor(private router: Router,private authenticationService: AuthenticationService) { }
     
     canActivate() {
         let isNotLogged:boolean = this.authenticationService.isNotLogged();
         if ( isNotLogged ) {
             return true;
         }
         this.router.navigate(['']);
         return false;
     }
 }