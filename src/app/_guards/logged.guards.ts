/**
 * Created by Andreï for ZKY.
 */
 import { Injectable } from '@angular/core';
 import { Router, CanActivate } from '@angular/router';

 import { AuthenticationService } from '../_services/authentication.service';
 
 @Injectable()
 export class LoggedGuard implements CanActivate {
     
     constructor(private router: Router,private authenticationService: AuthenticationService) { }
     
     canActivate() {
         let isLogged:boolean = this.authenticationService.isLogged();
         if ( isLogged ) {
             return true;
         }
         this.router.navigate(['/connection']);
         return false;
     }
 }