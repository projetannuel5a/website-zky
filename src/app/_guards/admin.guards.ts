/**
 * Created by Andreï for ZKY.
 */
 import { Injectable } from '@angular/core';
 import { Router, CanActivate } from '@angular/router';

 import { AuthenticationService } from '../_services/authentication.service';
 
 @Injectable()
 export class AdminGuard implements CanActivate {
     
     constructor(private router: Router,private authenticationService: AuthenticationService) { }
     
     canActivate() {
         let isAdmin:boolean = this.authenticationService.isAdmin();
         if ( isAdmin ) {
             return true;
         }
         this.router.navigate(['/connection']);
         return false;
     }
 }