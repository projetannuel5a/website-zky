/**
 * Created by Andreï for ZKY.
 */
 export class Account{
     admin: boolean;
     login: string;
     
     numDossier: number;
     nom: string;
     administration: string;
     compte: string;
     siret: string;
     enseigne: string;

     telephone1: string;
     telephone2: string;
     fax: string;
     email: string;

     cp: string;
     ville: string;
     pays: string;
     numVoie: string;
     nomdelavoie: string;
     complementAdresse: string;

     constructor(account:Account) {
         this.login = account.login;
         this.admin = account.admin;
         this.numDossier = account.numDossier;
         this.nom = account.nom;
         this.administration = account.administration;
         this.compte = account.compte;
         this.siret = account.siret;
         this.enseigne = account.enseigne;
         this.telephone1 = account.telephone1;
         this.telephone2 = account.telephone2;
         this.fax = account.fax;
         this.email = account.email;
         this.cp = account.cp;
         this.ville = account.ville;
         this.pays = account.pays;
         this.numVoie = account.numVoie;
         this.nomdelavoie = account.nomdelavoie;
         this.complementAdresse = account.complementAdresse;
     }
 }