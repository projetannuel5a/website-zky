/**
 * Created by Andreï for ZKY.
 */
 export class PaginationOptions {
     size:number;
     page:number;

     constructor(size:number, page:number) {
         this.size = size;
         this.page = page;
     }
 }