/**
 * Created by Andreï for ZKY.
 */
 export class CartInfos {
     id: number;
     status: string;
     validationDate: string;

     constructor(cartInfos:CartInfos) {
         this.id = cartInfos.id;
         this.status = cartInfos.status;
         this.validationDate = cartInfos.validationDate;
     }
 }