/**
 * Created by Andreï for ZKY.
 */
 import { Family } from '../_models/family';

 export class Subfamily {
     "id":number;
     "family":Family;
     "subfamily":number;
     "name":string;
 }