/**
 * Created by Andreï for ZKY.
 */
 import { Family } from '../_models/family';
 import { SubfamilyAndProducts } from '../_models/subfamilyAndProducts';

 export class FamilyAndSubfamilies {
     "family":Family;
     "subfamilyPublics":SubfamilyAndProducts[];
 }