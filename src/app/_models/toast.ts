/**
 * Created by Andreï for ZKY.
 */
 export class Toast {
     type:string;
     message:string;
     finished:boolean;

     constructor(type:string, message:string, finished:boolean) {
         this.type = type;
         this.message = message;
         this.finished = finished;
     }
 }