/**
 * Created by Andreï for ZKY.
 */
 import { Product } from '../_models/product';

 export class CartLine {
     productPublics: Product;
     quantity: number;

     constructor(cartLine:CartLine) {
         this.productPublics = new Product( cartLine.productPublics );
         this.quantity = cartLine.quantity;
     }

     getTotalPrice():number{
         return this.productPublics.prixdevente * this.quantity;;
     }
 }