/**
 * Created by Andreï for ZKY.
 */
 export class ProductFilters {
     family:number[];
     subfamily:number[];
     search:string;

     constructor(family:number[], subfamily:number[], search:string) {
         this.family = family;
         this.subfamily = subfamily;
         this.search = search;
     }
 }