/**
 * Created by Andreï for ZKY.
 */
 import { Product } from '../_models/product';
 import { Subfamily } from '../_models/subfamily';

 export class SubfamilyAndProducts {
     subfamily:Subfamily;
     products:Product[];
 }