/**
 * Created by Andreï for ZKY.
 */
 export class ChangePasswordInfos {
     oldPassword: string;
     newPassword: string;
 }