/**
 * Created by Andreï for ZKY.
 */
 import { Product } from '../_models/product';

 export class ProductsAndPagination {
     pageCount:number;
     productPublicList:Product[];
 }