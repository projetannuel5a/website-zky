/**
 * Created by Andreï for ZKY.
 */
 import { CartLine } from '../_models/cartLine';
 import { CartInfos } from '../_models/cartInfos';
 import { Account } from '../_models/account';

 export class Cart{
     cart:CartInfos;
     accountPublic:Account;
     cartLinePublics: CartLine[];

     constructor(cart:Cart) {
         this.cart = new CartInfos(cart.cart);
         this.accountPublic = new Account(cart.accountPublic);

         this.cartLinePublics = [];
         for(let i = 0 ; i < cart.cartLinePublics.length ; i++ ){
             this.cartLinePublics.push( new CartLine( cart.cartLinePublics[i] ) );
         }
         
     }

     getTotalPrice():number{
         let cartLines = this.cartLinePublics;
         let cartTotalPrice = 0;
         for(let i = 0; i < cartLines.length ; i++){
             cartTotalPrice += cartLines[ i ].getTotalPrice();
         }

         return cartTotalPrice;
     }
 }