/**
 * Created by Andreï for ZKY.
 */
 export class ContactInfos {
     "id":number;
     "address":string;
     "tel":string;
     "mail":string;
 }