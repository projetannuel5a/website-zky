export class Product {
    id:number;
    codearticle:string;
    img:string;
    ean13:string;
    intitule1:string;
    prixdachatbrut: number;
    pamp: number;
    prixdevente:number;
    ttc:string;
    unite:number;
    fournisseur:string;
    ventil:number;
    tva:number;
    qtesurfacture:string;
    libelleinterne:string;
    intitule2:string;
    intitule3:string;
    intitule4:string;
    nomenceurope:string;
    coderegroupt:string;

    constructor(product:Product) {
        this.id = product.id;
    this.img = product.img/**
 * Created by Andreï for ZKY.
 */
 ;
 this.codearticle = product.codearticle;
 this.ean13 = product.ean13;
 this.intitule1 = product.intitule1;
 this.prixdachatbrut =  product.prixdachatbrut;
 this.pamp =  product.pamp;
 this.prixdevente = product.prixdevente;
 this.ttc = product.ttc;
 this.unite = product.unite;
 this.fournisseur = product.fournisseur;
 this.ventil = product.ventil;
 this.tva = product.tva;
 this.qtesurfacture = product.qtesurfacture;
 this.libelleinterne = product.libelleinterne;
 this.intitule2 = product.intitule2;
 this.intitule3 = product.intitule3;
 this.intitule4 = product.intitule4;
 this.nomenceurope = product.nomenceurope;
 this.coderegroupt = product.coderegroupt;
}
}