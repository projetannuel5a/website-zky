/**
 * Created by Andreï for ZKY.
 */
 export class ClientCheckoutInfos {
     email: string;
     name: string;
     surname: string;
     company: string;
     phone: string;
 }