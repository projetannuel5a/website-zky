/**
 * Created by Andreï for ZKY.
 */
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'
 import { OnInit, Component } from '@angular/core'
 import {FormsModule} from '@angular/forms'
 import { Observable }        from 'rxjs/Observable';
 import { Subject }           from 'rxjs/Subject';

 import 'rxjs/add/observable/of';

 import 'rxjs/add/operator/catch';
 import 'rxjs/add/operator/debounceTime';
 import 'rxjs/add/operator/distinctUntilChanged';

 import { ProductsService } from '../_services/product.service';
 import { QueryParameterParserService } from '../_services/queryParameterParser.service';
 import { Product } from '../_models/product';
 import { ProductFilters } from '../_models/productFilters';
 import { FamilyAndSubfamilies } from '../_models/familyAndSubfamilies';
 import { SubfamilyAndProducts } from '../_models/subfamilyAndProducts';
 import { ProductsAndPagination } from '../_models/productsAndPagination';
 import { PaginationOptions } from '../_models/paginationOptions';

 @Component({
     selector: 'catalogue',
     styleUrls: ['./catalogue.component.scss'],
     templateUrl: './catalogue.component.html',
     providers: [ProductsService, QueryParameterParserService]
 })
 export class CatalogueComponent implements OnInit {
     constructor( 
         private _router: Router,
         private route: ActivatedRoute,
         private productsService: ProductsService,
         private queryParameterParserService: QueryParameterParserService
         ){

     }

     
     private maxPagesShown:number = 7;
     private pageCount:number = 1;

     private families: FamilyAndSubfamilies[];
     private products: Observable<Product[]>;

     private currentFamily: FamilyAndSubfamilies;
     private currentSubfamily: SubfamilyAndProducts;
     private currentSearchTerms: string = "";
     private currentPage:number = 1;

     private nextQueryIndex: number = 1;
     private lastQueryIndex: number = 0;

     private searchTermTimeout: NodeJS.Timer;
     private searchTermTimeoutDelay: number = 400;

  /**
   * Function called each time the URL change and is on the route 'catalogue'
   * Updates get families list if they are not stored yet then update filters and get the new product list
   * @return void
   */
   ngOnInit(): void {
       this.currentFamily = null;
       this.currentSubfamily = null;

       if( this.families !== null ){
           this.productsService.getFamilies().then(families =>{ 
               this.families = families;
               this.updateThenGetByFilters();
           });
       }
       else{
           this.updateThenGetByFilters();
       }
       
   }
  /**
   * Update filters and get the new product list
   * @return void
   */
   private updateThenGetByFilters(){
       var _self = this;
       this.route.params
       .forEach((params: Params) => {
           let decodedParamaters = _self.queryParameterParserService.decodeByRouteParameterName('params');
           if( decodedParamaters['family'] ){
               _self.updateFamilyById( decodedParamaters['family'] );
           }
           if( decodedParamaters['subfamily'] ){
               _self.updateSubfamilyById( decodedParamaters['subfamily'] );
           }
           if( decodedParamaters['searchTerms'] ){
               _self.updateSearchTerms( decodedParamaters['searchTerms'] );
           }
           if( decodedParamaters['page'] ){//page MUST be updated last
               _self.onPageChange( parseInt( decodedParamaters['page'] ) );
           }
           _self.getByFilters();
       });
   }

  /**
   * Get the new product list by filters,
   * handle if response is outdated with lastQueryIndex
   * @return void
   */
   private getByFilters(){
       let productFilters = this.getProductFilters();
       let paginationOptions = new PaginationOptions( 20, this.currentPage );

       let queryIndex:number = this.nextQueryIndex;
       this.nextQueryIndex++;
       this.productsService.getProductsByFilters( productFilters, paginationOptions )
       .then( response =>{
           if( queryIndex > this.lastQueryIndex ){
               this.lastQueryIndex = queryIndex;
               this.products = Observable.of<Product[]>(response.productPublicList);
               this.pageCount = response.pageCount;
           }
       });
   }

  /**
   * Called each time a parameter changes
   * @return void
   */
   private onFilterChange(): void{
       this.updateUrl();
   }

  /**
   * Get the current products filters (family, subfamily, search)
   * @return ProductFilters
   */
   private getProductFilters(): ProductFilters{
       let familyFilter = [];
       if( this.currentFamily ){
           familyFilter = [ this.currentFamily.family.family ];
       }

       let subfamilyFilter = [];
       if( this.currentSubfamily ){
           subfamilyFilter = [ this.currentSubfamily.subfamily.subfamily ];
       }

       return new ProductFilters( 
           familyFilter,
           subfamilyFilter,
           this.currentSearchTerms 
           );
   }

  /**
   * Called each time the page changes,
   * set the page
   * @param number page    the page number (first page is 1)
   * @return void
   */
   private onPageChange(page:number): void{
       if( this.currentPage !== page ){
           this.currentPage = page;
           this.onFilterChange();
       }
   }

  /**
   * Called each time the search changes,
   * Calls the update of the search term filter only if doesn't change in the next {searchTermTimeoutDelay} ms
   * @param string term    the search terms
   * @return void
   */
   private onSearchTermsChange(term: string): void {
       if( this.currentSearchTerms !== term ){
           this.currentSearchTerms = term;

           clearTimeout( this.searchTermTimeout );
           this.searchTermTimeout = setTimeout( () => { this.updateSearchTerms( term ); }, this.searchTermTimeoutDelay);
       }
   }

  /**
   * set the search term
   * @param string term    the search terms
   * @return void
   */
   private updateSearchTerms(term: string):void{
       this.currentSearchTerms = term;
       this.currentPage = 1;
       this.onFilterChange();
   }

  /**
   * get the family id from html select event and update the family
   * @param Event event    the html select event
   * @return void
   */
   private onFamilySelectChange( event:Event):void{
       let selectValue:string = (event.target as HTMLSelectElement).value;

       this.updateFamilyById( selectValue );
   }

  /**
   * call for update family using the id or null if the value was 'all'
   * @param string familyId    the family id
   * @return void
   */
   private updateFamilyById( familyId:string ):void{
       let nextCurrentFamily:FamilyAndSubfamilies = (familyId !== "all") ? this.getFamilyById( parseInt(familyId) ) : null;

       this.updateFamily( nextCurrentFamily );
   }

  /**
   * if the new family is different, set the family, the page to 1 and remove the subfamily
   * @param string familyId    the family id
   * @return void
   */
   private updateFamily( family:FamilyAndSubfamilies):void{
       if( this.currentFamily !== family ){
           this.currentPage = 1;
           this.setCurrentFamily(family);
           this.setCurrentSubfamily(null);
           this.onFilterChange();
       }
   }

  /**
   * get the subfamily id from html select event and update the subfamily
   * @param Event event    the html select event
   * @return void
   */
   private onSubfamilySelectChange( event:Event):void{
       let selectValue:string = (event.target as HTMLSelectElement).value;

       this.updateSubfamilyById( selectValue );
   }
  /**
   * call for update subfamily using the id or null if the value was 'all'
   * @param string subfamilyId    the subfamily id
   * @return void
   */
   private updateSubfamilyById( subfamilyId:string ):void{
       let nextCurrentSubfamily:SubfamilyAndProducts = (subfamilyId !== "all") ? this.getSubfamilyById( parseInt(subfamilyId) ) : null;

       this.updateSubfamily( nextCurrentSubfamily );
   }

  /**
   * if the new subfamily is different, set the subfamily
   * @param string familyId    the subfamily id
   * @return void
   */
   private updateSubfamily( subfamily:SubfamilyAndProducts):void{
       if( this.currentSubfamily !== subfamily ){
           this.currentPage = 1;
           this.setCurrentSubfamily(subfamily);
           this.onFilterChange();
       }
   }

  /**
   * find the family using its id
   * @param number familyId    the family id
   * @return FamilyAndSubfamilies
   */
   private getFamilyById(familyId:number):FamilyAndSubfamilies{
       for(let i = 0; i < this.families.length; i++){
           if( familyId == this.families[i].family.family ){
               return this.families[i];
           }
       }

       return null;
   }

  /**
   * find the subfamily using its id
   * @param number subfamilyId    the subfamily id
   * @return SubfamilyAndProducts
   */
   private getSubfamilyById(subfamilyId:number):SubfamilyAndProducts{
       let currentFamilySubfamilies = this.currentFamily.subfamilyPublics;
       for(let i = 0; i < currentFamilySubfamilies.length; i++){
           if( subfamilyId == currentFamilySubfamilies[i].subfamily.subfamily ){
               return currentFamilySubfamilies[i];
           }
       }

       return null;
   }

   private setCurrentFamily(family: FamilyAndSubfamilies){
       this.currentFamily = family;
   }

   private setCurrentSubfamily(subfamily: SubfamilyAndProducts){
       this.currentSubfamily = subfamily;
   }

  /**
   * Update URL with all the parameters using our custom query parameter manager
   * @return void
   */
   private updateUrl(){
       let params = {};
       if( this.currentFamily ){
           params['family'] = this.currentFamily.family.family.toString();
       }
       if( this.currentSubfamily ){
           params['subfamily'] = this.currentSubfamily.subfamily.subfamily.toString();
       }
       params['page'] = this.currentPage.toString();
       params['searchTerms'] = this.currentSearchTerms;

       this._router.navigate( ['/catalogue', this.queryParameterParserService.encode(params)] );
   }
}
