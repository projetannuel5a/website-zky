/**
 * Created by Andreï for ZKY.
 */
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'
 import { Input, Output, Component, EventEmitter } from '@angular/core'

 import { Product } from '../_models/product';

 @Component({
     selector: 'pagination',
     styleUrls: ['./pagination.component.scss'],
     templateUrl: './pagination.component.html'
 })
 export class PaginationComponent {
     constructor( 
         ){

     }

     @Input() private currentPage: number;
     @Input() private pageCount: number;
     @Input() private maxPagesShown: number;
     @Output() onPageChange: EventEmitter<any> = new EventEmitter();

     private onPageButtonClick(page: number): void {
         this.onPageChange.emit([page]);
     }

     private getPages():number[]{
         let maxPagesShownHalf:number = Math.floor(this.maxPagesShown/2);
         let firstPageShown:number = this.currentPage - maxPagesShownHalf;
         let lastPageShown:number = firstPageShown + this.maxPagesShown - 1;

         if( firstPageShown < 1){
             firstPageShown = 1;
             lastPageShown = this.maxPagesShown <= this.pageCount?this.maxPagesShown:this.pageCount;
         }

         if( lastPageShown > this.pageCount ){
             lastPageShown = this.pageCount;
             firstPageShown = (this.pageCount - this.maxPagesShown + 1) >= 1?this.pageCount - this.maxPagesShown + 1:1;
         }

         return Array( lastPageShown - firstPageShown + 1).fill(0).map((x,i) => i + firstPageShown );
     }

     private isCurrentPageTheFirst():boolean{
         return this.currentPage === 1;
     }

     private isFirstPageVisible():boolean{
         return this.getPages()[0] === 1;
     }

     private isCurrentPageTheLast():boolean{
         return this.currentPage === this.pageCount;
     }

     private isLastPageVisible():boolean{
         let pages = this.getPages();
         return pages[pages.length -1] === this.pageCount;
     }
 }
