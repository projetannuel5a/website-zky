/**
 * Created by Andreï for ZKY.
 */
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'
 import { OnInit, Component, trigger, state, style, transition, animate } from '@angular/core'

 import { CartService } from '../_services/cart.service';
 import { AuthenticationService } from '../_services/authentication.service';
 import { ToasterService } from '../_services/toaster.service';

 import { Cart } from '../_models/cart';
 import { CartLine } from '../_models/cartLine';

 @Component({
     selector: 'cart',
     styleUrls: ['./cart.component.scss'],
     templateUrl: './cart.component.html',
     providers: [CartService,AuthenticationService,ToasterService],
     animations: [
     trigger('slideInOut', [
         state('in', style({
             transform: 'translate3d(0, 0, 0)',
             boxShadow: "20px 0 35px 0 rgba(0, 0, 0, .1)"
         })),
         state('out', style({
             transform: 'translate3d(100%, 0, 0)',
             boxShadow: "-20px 0 35px 0 rgba(0, 0, 0, 0)"
         })),
         transition('in => out', animate('400ms ease-in-out')),
         transition('out => in', animate('400ms ease-in-out'))
         ]),
     ]
 })
 export class CartComponent implements OnInit {
     constructor( 
         private _router: Router,
         private route: ActivatedRoute,
         private cartService: CartService,
         private authenticationService: AuthenticationService
         ){

     }

     private cart: Cart;
     private cartTotalPrice: number;

     private menuState:string = 'out';

     ngOnInit(): void {

         if( this.authenticationService.isLogged() ){
             this.cartService.getCart()
             .then(cart => this.updateCart(cart) );
         }
     }

     private updateCart( cart:Cart ){
         this.cart = new Cart( cart );
     }

     private goToCheckout(): void {
         this.toggleCartMenu();
         this._router.navigate( ['/checkout'] );
     }

     private checkout(): void {
         this.toggleCartMenu();
         this.cartService.send().then( success => {
             if( success ){
                 ToasterService.popToast("info","Commande envoyée");
             }
             else{
                 ToasterService.popToast("error","Erreur lors de l'envoi");
             }
         }).catch( error =>{
             ToasterService.popToast("error","Erreur lors de l'envoi");
         });
     }

     private onClickCartLine( id : number): void {
         this._router.navigate( ['/product', id] );
     }

     private quantityDecrementAndSend( currentCartLine : CartLine){
         currentCartLine.quantity--;
         if( currentCartLine.quantity < 0 ){
             currentCartLine.quantity = 0;
         }
         this.cartService.setProductQuantity( currentCartLine.productPublics.id, currentCartLine.quantity )
         .then( cart => this.updateCart(cart) );
     }

     private quantityIncrementAndSend( currentCartLine : CartLine){
         currentCartLine.quantity++;
         this.cartService.setProductQuantity( currentCartLine.productPublics.id, currentCartLine.quantity )
         .then( cart => this.updateCart(cart) );
     }

     private toggleCartMenu(){
         this.menuState = this.menuState === 'out' ? 'in' : 'out';
         this.cartService.getCart()
         .then( cart => this.updateCart(cart) );
     }

     private numberTo2Decimals( number : number ){
         return ((number * 100) / 100).toFixed(2);
     }
 }
