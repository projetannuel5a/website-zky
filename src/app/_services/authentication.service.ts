/**
 * Created by Andreï for ZKY.
 */
 import { Injectable } from '@angular/core';
 import { Http, Headers, Response, RequestOptions } from '@angular/http';
  import { Router } from '@angular/router';
 import { Observable } from 'rxjs';
 import 'rxjs/add/operator/map';

 import { Account }           from '../_models/account';
 import { ContactInfos }           from '../_models/contactInfos';
 import { ChangePasswordInfos }           from '../_models/changePasswordInfos';

 import * as GlobalVariables from '../global-variables';

 @Injectable()
 export class AuthenticationService {
     private requestOptions: RequestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });

     public static token: string;
     public static currentUser: Account;
     
     constructor(private http: Http, private router: Router) {
         AuthenticationService.currentUser = JSON.parse(localStorage.getItem('currentUser')) as Account;
         AuthenticationService.token = JSON.parse(localStorage.getItem('token')) as string;
     }

     isLogged():boolean{
         if( AuthenticationService.currentUser && AuthenticationService.token){
             return true;
         }
         return false;
     }

     isNotLogged():boolean{
         return !this.isLogged();
     }

     isAdmin():boolean{
         if( AuthenticationService.currentUser && AuthenticationService.token){
             if( AuthenticationService.currentUser.admin ){
                 return true;
             }
         }
         return false;
     }
     
    /**
   * try to login, if it works, store the token and the current user so it can be used by other controllers
   * @param string login    login string
   * @param string password    password string
   * @return Promise<boolean>
   */
   login(login: string, password: string){
       return this.http.post( GlobalVariables.ApiBasePath + '/account/authenticate', JSON.stringify({ login: login, password: password }), this.requestOptions )
       .toPromise()
       .then( response => {
           let token = response.json() && response.json().token;
           if (token) {
               AuthenticationService.token = token;
               localStorage.setItem('token', JSON.stringify(token));

               this.http.post( GlobalVariables.ApiBasePath + '/account/my-account', JSON.stringify({ token: token }), this.requestOptions )
               .toPromise()
               .then( response => {
                   let currentUser = response.json() && response.json().accountPublic;
                   if (currentUser) {
                       AuthenticationService.currentUser = currentUser;
                       localStorage.setItem('currentUser', JSON.stringify(currentUser));
                       return true;
                   } else {
                       return false;
                   }
               })
               .catch(this.handleError);

               return true;
           } else {
               return false;
           }
       })
       .catch(this.handleError);
   }

   changePassword(changePasswordInfos: ChangePasswordInfos): Promise<any> {
       return this.http
       .post(`${GlobalVariables.ApiBasePath}/account/password`, JSON.stringify({token:AuthenticationService.token,body:changePasswordInfos}), this.requestOptions )
       .toPromise()
       .then(response => {
           this.login(AuthenticationService.currentUser.login, changePasswordInfos.newPassword);
           return response.json() as any;
       })
       .catch(this.handleError);
   }

   forgottenPassword(email: string): Promise<any> {
       return this.http
       .post(`${GlobalVariables.ApiBasePath}/account/restore`, JSON.stringify({email:email}), this.requestOptions )
       .toPromise()
       .then(response => response.json() as any)
       .catch(this.handleError);
   }

   updateAccount(account: Account): Promise<Account> {
       return this.http
       .post(`${GlobalVariables.ApiBasePath}/account/update`, JSON.stringify({token:AuthenticationService.token,body:account}), this.requestOptions )
       .toPromise()
       .then(response => {
           let currentUser = response.json() as Account;
           if (currentUser) {
               AuthenticationService.currentUser = currentUser;
               localStorage.setItem('currentUser', JSON.stringify(currentUser));

               return currentUser}
           })
       .catch(this.handleError);
   }

   getContactInfos(): Promise<ContactInfos> {
       return this.http
       .get(`${GlobalVariables.ApiBasePath}/account/info`, this.requestOptions )
       .toPromise()
       .then(response => response.json() as ContactInfos)
       .catch(this.handleError);
   }

   setContactInfos(contactInfos:ContactInfos): Promise<ContactInfos> {
       return this.http
       .post(`${GlobalVariables.ApiBasePath}/account/info`, JSON.stringify({token:AuthenticationService.token,body:contactInfos}), this.requestOptions )
       .toPromise()
       .then(response => response.json() as ContactInfos)
       .catch(this.handleError);
   }
   
   logout(): void {
       AuthenticationService.token = null;
       localStorage.removeItem('token');
       AuthenticationService.currentUser = null;
       localStorage.removeItem('currentUser');
       this.router.navigate(['/']);

   }

   private handleError(error: any): Promise<any> {
       console.error('An error occurred', error);
       return Promise.reject(error.message || error);
   }
}