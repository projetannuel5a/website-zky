/**
 * Created by Andreï for ZKY.
 */
 import { Injectable } from '@angular/core';
 import { Subject }    from 'rxjs/Subject';


 import { Toast }    from '../_models/toast';
 
 @Injectable()
 export class ToasterService {
     
     // Observable string sources
     private static toast = new Subject<Toast>();
     private static toastDuration: number = 3000;
     private static currentTimeout: NodeJS.Timer;
     
     // Observable string streams
     static toast$ = ToasterService.toast.asObservable();
     
     // Service message commands
     static popToast(type: string,message: string) {
         ToasterService.toast.next( new Toast( type, message, false ));
         clearTimeout( ToasterService.currentTimeout );
         ToasterService.currentTimeout = setTimeout( () => {
             ToasterService.toast.next( new Toast( type, message, true ));
         },ToasterService.toastDuration);
     }

     static init() {
         ToasterService.toast.next( new Toast( "", "", true ));
     }
 }