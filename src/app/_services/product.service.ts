/**
 * Created by Andreï for ZKY.
 */
 import { Injectable }    from '@angular/core';
 import { Headers, Http, RequestOptions } from '@angular/http';

 import { Observable }     from 'rxjs/Observable';
 import 'rxjs/add/operator/map';
 import 'rxjs/add/operator/toPromise';
 
 import { Product }           from '../_models/product';
 import { ProductFilters }           from '../_models/productFilters';
 import { FamilyAndSubfamilies } from '../_models/familyAndSubfamilies';
 import { ProductsAndPagination } from '../_models/productsAndPagination';
 import { PaginationOptions } from '../_models/paginationOptions';

 import * as GlobalVariables from '../global-variables';

 @Injectable()
 export class ProductsService {

     private headers = new Headers({'Content-Type': 'application/json'});
     private requestOptions: RequestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
     private maxProductsPerPage = 20;

     constructor(private http: Http) { }

     getProduct(id: number): Promise<Product> {
         const url = `${GlobalVariables.ApiBasePath}/product/${id}`;
         return this.http.get(url)
         .toPromise()
         .then( response => response.json() as Product)
         .catch(this.handleError);
     }

     getProducts(): Promise<Product[]> {
         const url = `${GlobalVariables.ApiBasePath}/product`;
         return this.http.get(url)
         .toPromise()
         .then( response => response.json() as Product[])
         .catch(this.handleError);
     }

     getFamilies(): Promise<FamilyAndSubfamilies[]> {
         const url = `${GlobalVariables.ApiBasePath}/product/family`;
         return this.http.get(url)
         .toPromise()
         .then( response => response.json() as FamilyAndSubfamilies[])
         .catch(this.handleError);
     }

     getProductsByFilters(productFilters: ProductFilters, paginationOptions: PaginationOptions): Promise<ProductsAndPagination> {
         return this.http
         .post(`${GlobalVariables.ApiBasePath}/product/filter?page=${paginationOptions.page - 1}&size=${paginationOptions.size}`, JSON.stringify(productFilters), this.requestOptions )
         .toPromise()
         .then(response => response.json() as ProductsAndPagination)
         .catch(this.handleError);
     }

     private handleError(error: any): Promise<any> {
         console.error('An error occurred', error);
         return Promise.reject(error.message || error);
     }
 }