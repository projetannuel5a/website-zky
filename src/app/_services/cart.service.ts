/**
 * Created by Andreï for ZKY.
 */
 import { Injectable }    from '@angular/core';
 import { Headers, Http, RequestOptions } from '@angular/http';

 import 'rxjs/add/operator/toPromise';
 
 import { AuthenticationService }           from './authentication.service';
 import { Cart }           from '../_models/cart';
 import { ClientCheckoutInfos } from '../_models/clientCheckoutInfos';

 import * as GlobalVariables from '../global-variables';

 @Injectable()
 export class CartService {

     private requestOptions: RequestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });

     constructor(private http: Http) { }

     getCart(): Promise<Cart> {
         const url = `${GlobalVariables.ApiBasePath}/cart`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart)
         .catch(this.handleError);
     }

     getHistory(): Promise<Cart[]> {
         const url = `${GlobalVariables.ApiBasePath}/cart/history`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart[])
         .catch(this.handleError);
     }

     addProductQuantity(productId :number,quantity : number): Promise<Cart> {
         const url = `${GlobalVariables.ApiBasePath}/cart/add`;
         return this.http.post(url, JSON.stringify({
             token:AuthenticationService.token,
             body:{
                 productId:productId,
                 quantity:quantity
             }
         }
         ), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart)
         .catch(this.handleError);
     }

     setProductQuantity(productId :number,quantity : number): Promise<Cart> {
         const url = `${GlobalVariables.ApiBasePath}/cart/set`;
         return this.http.post(url, JSON.stringify({
             token:AuthenticationService.token,
             body:{
                 productId:productId,
                 quantity:quantity
             }
         }
         ), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart)
         .catch(this.handleError);
     }

     send(): Promise<Boolean> {
         const url = `${GlobalVariables.ApiBasePath}/cart/validate`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Boolean)
         .catch(this.handleError);
     }

     public getDoingCarts() {
         const url = `${GlobalVariables.ApiBasePath}/cart/doing`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart[])
         .catch(this.handleError);
     }

     public getPendingCarts() {
         const url = `${GlobalVariables.ApiBasePath}/cart/pending`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart[])
         .catch(this.handleError);
     }

     public getValidatedCarts() {
         const url = `${GlobalVariables.ApiBasePath}/cart/validated`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart[])
         .catch(this.handleError);
     }

     public getCartById(cartId:number) {
         const url = `${GlobalVariables.ApiBasePath}/cart/${cartId}`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as Cart)
         .catch(this.handleError);
     }

     public setCartToValidated(cartId:number) {
         const url = `${GlobalVariables.ApiBasePath}/cart/validated/${cartId}`;
         return this.http.post(url, JSON.stringify({token:AuthenticationService.token}), this.requestOptions )
         .toPromise()
         .then( response => response.json() as boolean)
         .catch(this.handleError);
     }

     private handleError(error: any): Promise<any> {
         console.error('An error occurred', error);
         return Promise.reject(error.message || error);
     }
 }

