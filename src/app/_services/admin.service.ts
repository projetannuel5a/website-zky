/**
 * Created by Andreï for ZKY.
 */
 import { Injectable }    from '@angular/core';
 import { Headers, Http, RequestOptions } from '@angular/http';

 import { Observable }     from 'rxjs/Observable';
 import 'rxjs/add/operator/map';
 import 'rxjs/add/operator/toPromise';

 import { Cart }     from '../_models/cart';

 import { AuthenticationService }     from '../_services/authentication.service';

 import * as GlobalVariables from '../global-variables';
 
 @Injectable()
 export class AdminService {

     private requestOptions: RequestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });

     constructor(private http: Http) { }

   /**
   * send the new products csv and server will update all products
   * @param FormData formData    formData containing the new products csv exported by Quadra
   * @return Promise<boolean>
   */
   public updateProducts(formData: FormData) {
       const url = `${GlobalVariables.ApiBasePath}/product/upload`;

       let headers = new Headers();
       headers.append('Accept', 'application/json');
       let options = new RequestOptions({ headers: headers });
       formData.append("token", AuthenticationService.token);

       return this.http.post( url, formData, options)
       .toPromise()
       .then(response => response.json() as boolean)
       .catch(this.handleError);
   }

  /**
   * send the new clients csv and server will update all clients
   * @param FormData formData    formData containing the new clients csv exported by Quadra
   * @return Promise<boolean>
   */
   public updateClients(formData: FormData) {
       const url = `${GlobalVariables.ApiBasePath}/account/upload`;

       let headers = new Headers();
       headers.append('Accept', 'application/json');
       let options = new RequestOptions({ headers: headers });
       formData.append("token", AuthenticationService.token);

       return this.http.post( url, formData, options)
       .toPromise()
       .then(response => response.json() as boolean)
       .catch(this.handleError);
   }

   private debugTest = function(response){
       return response.json().data as Object;
   }

   private handleError(error: any): Promise<any> {
       console.error('An error occurred', error);
       return Promise.reject(error.message || error);
   }
}

