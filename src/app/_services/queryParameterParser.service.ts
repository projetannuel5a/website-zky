/**
 * Created by Andreï for ZKY.
 */
 import { Injectable } from '@angular/core';
 import { Router, ActivatedRoute, Params } from '@angular/router';
 
 @Injectable()
 export class QueryParameterParserService {
     private subparameterSeparator = '+';
     private subparameterSeparatorEscaped = '$plus$';
     private keyValueSeparator = '-';
     private keyValueSeparatorEscaped = '$minus$';
     
     constructor(private _router: Router,private route: ActivatedRoute,) {
     }

    /**
   * decode the parameter {parameterName} in the url
   * @param string parameterName    name of the parameter in the url from the route configuration
   * @return Object
   */
   public decodeByRouteParameterName(parameterName:string):Object{
       let parsedParameter = {};
       this.route.params
       .forEach((params: Params) => {
           let parameter = params[parameterName];
           if( parameter ){
               parsedParameter = this.decode(parameter);
           }
       });

       return parsedParameter;
   }

    /**
   * decode the given parameter string
   * @param string parameterString    string of encoded parameter
   * @return Object
   */
   public decode(parameterString:string):Object{
       let parsedParameter = {};
       let subparameters = parameterString.split(this.subparameterSeparator);
       let subparameterSplit = [];
       for(let i = 0; i < subparameters.length; i++ ){
           subparameterSplit = subparameters[i].split( this.keyValueSeparator );
           if( subparameterSplit.length === 2 && subparameterSplit[1] !== "" ){
               let subparameterKey = this.unescapeSeparators( subparameterSplit[0] );
               let subparameterValue = this.unescapeSeparators( subparameterSplit[1] );

               parsedParameter[ subparameterKey ] = subparameterValue;
           }
       }
       return parsedParameter;
   }

    /**
   * encode the given object
   * @param Object parameterObject    Object of the parameter to encode
   * @return string
   */
   public encode(parameterObject:Object):string{
       let encodedParameter = "";
       let encodedSubparameters = [];
       
       for( var property in parameterObject ){
           if( parameterObject.hasOwnProperty( property ) && parameterObject[property] !== ""){
               let subparameterEscapedKey = this.escapeSeparators( property );
               let subparameterEscapedValue = this.escapeSeparators( parameterObject[property] );
               
               encodedSubparameters.push( subparameterEscapedKey + this.keyValueSeparator + subparameterEscapedValue );
           }
       }
       encodedParameter = encodedSubparameters.join( this.subparameterSeparator );

       return encodedParameter;
   }

    /**
   * unescape the separators in the given subparameter string
   * @param string string    encoded subparameter
   * @return string
   */
   private unescapeSeparators(string:string):string{
       string = string.replace( this.subparameterSeparatorEscaped, this.subparameterSeparator );
       string = string.replace( this.keyValueSeparatorEscaped, this.keyValueSeparator );

       return string;
   }

     /**
   * escape the separators in the given subparameter string
   * @param string string    subparameter
   * @return string
   */
   private escapeSeparators(string:string):string{
       string = string.replace( this.subparameterSeparator, this.subparameterSeparatorEscaped );
       string = string.replace( this.keyValueSeparator, this.keyValueSeparatorEscaped );

       return string;
   }
}