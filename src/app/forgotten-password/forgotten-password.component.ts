/**
 * Created by Andreï for ZKY.
 */
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params, NavigationEnd } from '@angular/router'
 import { OnInit, Component} from '@angular/core'

 import { AuthenticationService } from '../_services/authentication.service';
 import { ToasterService } from '../_services/toaster.service';
 import { AlertService } from '../_services/alert.service';
 import { ConnectionInfos } from '../_models/connectionInfos';


 @Component({
     selector: 'forgotten-password',
     styleUrls: ['./forgotten-password.component.scss'],
     templateUrl: './forgotten-password.component.html',
     providers: [AuthenticationService, AlertService, ToasterService]
 })
 export class ForgottenPasswordComponent {
     constructor( 
         private router: Router,
         private route: ActivatedRoute,
         private authenticationService: AuthenticationService,
         private alertService: AlertService
         ){
     }

     private email : string= "";

     forgottenPasswordSend(){
         this.authenticationService.forgottenPassword(this.email)
         .then( connectionSuccess => {
             if( connectionSuccess ){
                 ToasterService.popToast("info","Vous allez recevoir un email de confirmation");
                 this.router.navigate(["/"]);
             }
         })
         .catch( error =>{
             this.alertService.message("error", "Erreur : Aucun compte n'existe avec cet email");
         });
     }

     ngOnInit(): void {
     }
 }