/**
 * Created by Andreï for ZKY.
 */
 import {Component, OnInit} from '@angular/core';
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'
 import { CartService } from '../../_services/cart.service';
 import { Cart } from '../../_models/cart';


 @Component({
     selector: 'carts-management',
     styleUrls: ['./carts-management.component.scss'],
     templateUrl: './carts-management.component.html',
     providers:[CartService]
 })
 export class CartsManagementComponent implements OnInit {
     constructor( 
         private cartService: CartService,
         private _router: Router
         ){}

     private carts:Cart[];
     private currentStatus:string;

     ngOnInit() {
         this.currentStatus = "PENDING";
         this.cartService.getPendingCarts().then( (carts:Cart[]) => this.updateCart(carts) );
     }

     private updateCart( carts:Cart[]){
         this.carts = [];
         for( let i = 0 ; i < carts.length ; i++ ){
             this.carts.push( new Cart( carts[i] ) );
         }
     }

     private onStatusSelectChange( event:Event):void{
         let selectValue:string = (event.target as HTMLSelectElement).value;
         this.currentStatus = selectValue;

         switch (selectValue) {
             case "DOING":
             this.cartService.getDoingCarts().then( (carts:Cart[]) => this.updateCart(carts) );
             break;
             case "PENDING":
             this.cartService.getPendingCarts().then( (carts:Cart[]) => this.updateCart(carts) );
             break;
             case "VALIDATED":
             this.cartService.getValidatedCarts().then( (carts:Cart[]) => this.updateCart(carts) );
             break;

         }
     }

     private onClickPreview( id ): void {
         this._router.navigate( ['/cart-view', id] );
     }

     private numberTo2Decimals( number : number ){
         return ((number * 100) / 100).toFixed(2);
     }
 }