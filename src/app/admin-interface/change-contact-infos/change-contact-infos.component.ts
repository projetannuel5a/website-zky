/**
 * Created by Andreï for ZKY.
 */
 import {Component, OnInit} from '@angular/core';
 import { Router } from '@angular/router';
 import { ActivatedRoute, Params } from '@angular/router'

 import { AlertService } from '../../_services/alert.service';
 import { AuthenticationService } from '../../_services/authentication.service';
 import { ToasterService } from '../../_services/toaster.service';

 import { ContactInfos } from '../../_models/contactInfos';


 @Component({
     selector: 'change-contact-infos',
     styleUrls: ['./change-contact-infos.component.scss'],
     templateUrl: './change-contact-infos.component.html',
     providers:[AuthenticationService,AlertService,ToasterService]
 })
 export class ChangeContactInfosComponent {
     constructor( 
         private authenticationService: AuthenticationService,
         private _router: Router,
         private alertService: AlertService,
         private toasterService: ToasterService
         ){}

     private contactInfos : ContactInfos;

     ngOnInit() {
         this.authenticationService.getContactInfos()
         .then( contactInfos => {
             this.contactInfos = contactInfos;
         });
     }

     private onClickSend( id ): void {
         this.authenticationService.setContactInfos( this.contactInfos )
         .then( contactInfos => {
             this.contactInfos = contactInfos;
             ToasterService.popToast("info","Informations de contact mises à jour");
         }).catch( error =>{
             ToasterService.popToast("error","Erreur lors de la modification");
         });
     }
 }