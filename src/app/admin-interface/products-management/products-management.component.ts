/**
 * Created by Andreï for ZKY.
 */
 import {Component} from '@angular/core';
 import { AdminService } from '../../_services/admin.service';
 import { ToasterService } from '../../_services/toaster.service';


 @Component({
     selector: 'products-management',
     styleUrls: ['./products-management.component.scss'],
     templateUrl: './products-management.component.html',
     providers:[AdminService,ToasterService]
 })
 export class ProductsManagementComponent {
     constructor( 
         private adminService: AdminService
         ){}

     private file:File;

     fileChange(event) {
         let fileList: FileList = event.target.files;
         if(fileList.length > 0) {
             this.file = fileList[0];
         }
     }

     sendFile(){
         let formData = new FormData();
         formData.append('file', this.file, this.file.name);
         this.adminService.updateProducts( formData ).then( success => {
             if( success ){
                 ToasterService.popToast("info","Liste des produits mise à jour");
             }
             else{
                 ToasterService.popToast("error","Erreur lors de la modification");
             }
         }).catch( error =>{
             ToasterService.popToast("error","Erreur lors de la modification");
         });
     }
 }
