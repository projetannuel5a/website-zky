/**
 * Created by Andreï for ZKY.
 */
 import {Component} from '@angular/core';
 import { AdminService } from '../../_services/admin.service';
 import { ToasterService } from '../../_services/toaster.service';


 @Component({
     selector: 'clients-management',
     styleUrls: ['./clients-management.component.scss'],
     templateUrl: './clients-management.component.html',
     providers:[AdminService,ToasterService]
 })
 export class ClientsManagementComponent {
     constructor( 
         private adminService: AdminService
         ){}

     private file:File;

     fileChange(event) {
         let fileList: FileList = event.target.files;
         if(fileList.length > 0) {
             this.file = fileList[0];
         }
     }

     sendFile(){
         let formData = new FormData();
         formData.append('file', this.file, this.file.name);
         this.adminService.updateClients( formData ).then( success => {
             if( success ){
                 ToasterService.popToast("info","Liste des clients mise à jour");
             }
             else{
                 ToasterService.popToast("error","Erreur lors de la modification");
             }
         }).catch( error =>{
             ToasterService.popToast("error","Erreur lors de la modification");
         });
     }
 }
